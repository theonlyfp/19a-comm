# Nmap

## Introduction

Nmap ("Network Mapper") is a free and open source utility for network discovery and security auditing. 
Nmap uses raw IP packets in different ways to determine what hosts are available on the network. 
These packets are stealthy by default, in the sense that no connection is established. 
Usually logs are only kept on succesful connections or login attempts. Some firewalls or antiviruses will however try to detect attempted connections and prevent portscanning. 
Services (application name and version) the hosts are offering are also available to be determined on the scanned ports. 
Operating systems (and OS versions) can be detected but 1 open and 1 closed port is needed for the OS to be detected accuartely.
Nmap can determine if any filters/firewalls are in use on the scanned ports. 
It includes parameters to spoof both IP and MAC address which will make it difficult to track or block scanning from the attacker. Decoy packets are also an option to confuse the target.
It was designed to rapidly scan large networks, but works fine against single hosts. 


## Using Nmap

Nmap has a wide amount of options and parameters for scanning network. The default scan is made to be fast and a easy to use accross a network. 
A more advance scan called Idle scan is also an option for even more stealthy network scans.

### Default scan **namp \<target subnet\>/\<target subnet mask\>**
The default scan is easy to use as a fast scan of a network.
It is a combination of a ARP request accross the network, a DNS reverse lookup, and a SYN stealth scan (**-sS**).

#### Step-by-Step of default scan
* A wide ARP request accross the designated network is used to discover hosts who is up.
* A reverse DNS lookup is used to determine if any IP addresses prefers the domain name istead of the addociated IP.
* All active hosts are used for the SYN stealth scan on the 1000 most commonly used ports.
* The active ports are displayed along with the service provided at that port for each host, along with the host's MAC address. 

### Zombie / Idle scan **-sI \<zombie host\[:probeport\]\>**
 
The Zombie / Idle scan works by using an extra host as a proxy for network mapping. 
This way port scanning is spoofed to come from the zombie and not the attacker. 
The MAC address is shown in wireshark to be from the attacker but the IP address is the zombie.
The zombie host is idealy an idle host with global IPID increments e.g. a printer, windows 98, etc.
It is important the zombie isn't creating traffic on its own as this tampers with the global IPID increments will make closed ports appear to be open or create an error for Nmap.

#### Step-by-Step of Zombie / Idle scan
* A SYN/ACK packet is sent to the zombie which returns an RST packet back disclosing its IPID count.
* A SYN packet with the zombie's IP and designated port is sent from the attacker to the target.
* The target responds either with a RST packet (port down) or a SYN/ACK packet (port up) to the zombie.
* The zombie will not respond to the RST packet (port down) but will return an RST packet itself if it recieves a SYN/ACK packet (port up) as it wasn't expecting it.
* A new SYN/ACK packet is sent from the attacker to the zombie and the new IPID is disclosed. 
* If the IPID is increased by 1, the port is either down or filtered. 
* If the IPID is increased by 2, the port is up.

## Syntax (Extra parameters)

Nmap has a lot of parameters and different options for scanning networks. 
Consult the Links provided for even more parameters and more in depth explanation of Nmap's options on network mapping.

### SYN stealth scan **-sS**

This type of scan uses TCP to check if a port is open. 
It is also called a half-open scan as no connection is ever created but the target will acknowledge the connection attempt by the attacker.

#### Step-by-Step of SYN stealth scan
* The attacker sends a TCP SYN packet.
* The target sends back either: an ACK SYN packet (port up), or a ACK RST (port down).
* The attacker sends back a RST packet and no connection is created.

### Version detection **-sV**

Version detection works by actually connecting to the open port. This gives a response with the service running on the port.

### OS Fingerprinting **-O**

OS Fingerprinting is able to determin the operating system on the target. This works by sending and comparing TCP/IP packets and comparing to a OS database which follows with Nmap. 

### Spoof IP address **-S**

Spoofing the IP address makes it seem that the packets are comming from a different source.
When spoofing the source address, Nmap also needs to know what interface to use for sending packages. This is done with the option **-e \<interface\>**.
A specific IP address is needed that also follows the subnet mask to be able to communicate on the subnet. 

### Spoof MAC address **--spoof-mac \<mac address/prefix/vendor name\>**

Spoofing the MAC address along with the IP spoof will make it completely look like the packets are comming from a different source.
A specific MAC address can be used or if using **--spoof-mac 0** a random MAC address will be used.

### Timing **-T\<0-5\>**

Most modern firewalls and antiviruses will prevent rapid portscanning. To prevent the detection of a continous portscan of a host timing can be used.
Refer to [Nmap timing page](https://nmap.org/book/performance-timing-templates.html) for timing on different levels. 

## Example of Nmap scan

A basic network scan:
* nmap \<target subnet\>/\<target subnet mask\> 

An OS scan and port service scan:
* nmap -O -sV \<target subnet\>/\<target subnet mask\>

An advanced scan with a spoofed IP and MAC address:
* nmap -Pn -S \<spoof source address\> -e \<internet interface\> --spoof-mac 0 \<target subnet\>/\<target subnet mask\>

A scan using a zombie host as decoy:
* nmap -Pn -sI \<zombie host\[:probeport\]\> \<target subnet\>/\<target subnet mask\>

## Links

Nmap website https://nmap.org/  
Nmap kali https://tools.kali.org/information-gathering/nmap  
Nmap tutorial https://nmap.org/bennieston-tutorial/  
Nmap Idle scan https://nmap.org/book/idlescan.html  
Nmap timing https://nmap.org/book/performance-timing-templates.html  